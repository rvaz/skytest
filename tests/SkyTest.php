<?php

/**
 * Created by PhpStorm.
 * User: Vaz
 * Date: 18/02/2016
 * Time: 2:42
 */

require_once '../app/model/ModelFile.php';
require_once '../FileManipulation/File.php';

/**
 * Class SkyTest
 * ATTENTION there is insertion in two different functions testKeepDataFile and testRecordData
 * so when look in the data.txt file will have 3 registers
 */
class SkyTest extends PHPUnit_Framework_TestCase{


    /**
     * Test record data in file
    */
    function testKeepDataFile(){
        $modelFile=\FileManipulation\File::getInstance();
        $modelFile->setLocalFile('../app/file/data.txt');
        //clear data.txt
        ftruncate($modelFile->getLocalFile(), 0);

        // insert data from person 1
        $personName = array(array('firstName' => 'Rick','surName' => 'Martin'));
        $modelFile->updateFile($personName);
        // insert data from person 2
        $modelFile->setLocalFile('../app/file/data.txt');
        $personName2 = array(array('firstName' => 'John','surName' => 'Ford'));
        $modelFile->updateFile($personName2);

        $dataSimulation = 'Rick,Martin'.PHP_EOL.'John,Ford';

        //read data file to confer with expected information
        $modelFile=\FileManipulation\File::getInstance();
        $modelFile->setLocalFile('../app/file/data.txt');
        $this->assertEquals($dataSimulation.PHP_EOL,$modelFile->listData());
    }

    /**
     *Test recording a first and surname on file
     */
    function testRecordData(){
        $modelFile=\FileManipulation\File::getInstance();
        $modelFile->setLocalFile('../app/file/data.txt');
        $personName = array(array('firstName' => 'Sky','surName' => 'Net'));
        $this->assertEquals('',$modelFile->updateFile($personName));
    }

    /**
     *Test checking if file was created after call testRecordData
     */
    /**
     * @depends testRecordData
     */
    public function testFailure() {
        $this->assertFileExists('../app/file/data.txt');
    }
}
