<?php
/**
 * Created by PhpStorm.
 * User: Vaz
 * Date: 18/02/2016
 * Time: 10:42
 */


/**
* Class responsible for manipulate data information and redirect it to be recorded
 */
class ModelFile {
    /**
     * @var keep the value of instantiated file class (FileManipulation\File)
     */
    private $file;

    /**
     * @param array $dataName
     */
    public function recordData($dataName){
        /** Call singleton File  */
        $this->file = FileManipulation\File::getInstance();
        $this->file->setLocalFile('../file/data.txt');
        $this->file->updateFile($dataName);
    }

    /**
     * @return string
     */
    public function listDataFile(){
        $this->file = FileManipulation\File::getInstance();
        $this->file->setLocalFile('../file/data.txt');
        return $this->file->getListData();
    }
}


?>