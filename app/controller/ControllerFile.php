<?php
/**
 * Created by PhpStorm.
 * User: Vaz
 * Date: 18/02/2016
 * Time: 10:36
 */
require "../../vendor/autoload.php";

class controllerFile {
    // DELEGATE PATTERN to call the class responsible for record data
    public function doAction($action,$dataName){
        switch($action) {
            case 'record': {
                $modelFile = new ModelFile();
                $modelFile->recordData($dataName);
            }break;

            default:
                return null;
        }
    }
    public function __construct(){}
}
/* Get the POST information from index.php to redirect */
if (isset($_POST['action'])  && isset($_POST['dataNames'])) {
    /* Undone the stringify and the parse made on function.js to transfer information and get back the array with the data*/
    //$dataName = json_decode($_POST["dataNames"], true);
    $dataName = $_POST["dataNames"];
    $action = $_POST['action'];
    $controllerFile = new controllerFile($action,$dataName);
    $controllerFile->doAction($action,$dataName);
}



?>