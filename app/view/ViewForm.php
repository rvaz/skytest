<?php

/**
 * Created by PhpStorm.
 * User: Vaz
 * Date: 19/02/2016
 * Time: 02:24
 */
?>
<html>
    <head lang="en">
        <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="css/sky.css">
        <title>Register Load Files</title>
    </head>
    <body>
    <form method="post" name="formData">
        <table id="testTable">
            <tr>
                <th>First name</th>
                <th>Last name</th>
            </tr>
            <tr>
                <td><input type="text" name="people[][firstname]" value="Jeff" /></td>
                <td><input type="text" name="people[][surname]" value="Stelling" /></td>
            </tr>
            <tr>
                <td><input type="text" name="people[][firstname]" value="Chris" /></td>
                <td><input type="text" name="people[][surname]" value="Kamara" /></td>
            </tr>
            <tr>
                <td><input type="text" name="people[][firstname]" value="Alex" /></td>
                <td><input type="text" name="people[][surname]" value="Hammond" /></td>
            </tr>
            <tr>
                <td><input type="text" name="people[][firstname]" value="Jim" /></td>
                <td><input type="text" name="people[][surname]" value="White" /></td>
            </tr>
            <tr>
                <td><input type="text" name="people[][firstname]" value="Natalie" /></td>
                <td><input type="text" name="people[][surname]" value="Sawyer" /></td>
            </tr>
        </table>
        <input type="submit" value="Register" id="Register"/>
    </form>
    <p>&nbsp;</p>
    <div id="divResult"></div>
    </body>
</html>