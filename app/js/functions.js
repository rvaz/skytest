/**
 Name: functions.js
 */

/***
 * Clear the unnecessary attributes from the table*/
function removeUnnecessary(Elements) {
    Elements.each(function () {
        var element = $(this);
        element.replaceWith(element.html());
    })
}

$(document).ready(function(){
    $("#Register").click(function(){

        var arrayNames = [];

        removeUnnecessary($("table tr em"));
        $("#testTable tr").each(function (index) {
            if (index !== 0) {
                var row = $(this);
                var firstName = row.find("td:first input");
                var surName = row.find("td:eq(1) input");
                var temp = {'firstName':firstName.val(),'surName':surName.val()};
                arrayNames.push(temp);
            }
        });
        if (arrayNames.length > 0){
            /**
             * Use JSON to turn the array to string and transfer the information safely
             * */
            recordRegisters(JSON.parse(JSON.stringify(arrayNames)));
        }
    });
});
/* Call controller class to transmit the form information  using post  */
function recordRegisters(arrayNames){
    $("#divResult").html("Updating register...");
    $.post("controller/ControllerFile.php",
        {action:'record', dataNames: arrayNames},
        function(result){
            console.log(result);
            $("#divResult").html(result);
        }
    );
}
