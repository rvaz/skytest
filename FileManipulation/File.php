<?php
 /**
  * @author Vaz
  *
  */
/** Used namespace to made this file be reached by the files in app path*/
namespace FileManipulation;

/**
 * Class File
 *
 * Class responsible for manipulate disk files
 *
 * @package FileManipulation
 */
class File{
     /*** @var */
     private static $_instance;
     /*** @var string */
     private $localFile = NULL;

    /**
     * @return string
     */
    public function getLocalFile() {
        return $this->localFile;
    }

    /**
     * @param string $localFile
     */
    public function setLocalFile($localFile) {
        $this->localFile = fopen($localFile, "a+") or die("Unable to open/read/write file!");
    }

     /*** @param array $dataName */
     function updateFile($dataName){
         $dataToFile = '';
         foreach($dataName  as $row){
             $dataToFile .= $row["firstName"].",".$row["surName"].PHP_EOL;
         }
         fwrite($this->getLocalFile(), $dataToFile);
         $this->closeLocalFile();
	}

    /**
     * @return string
     */
    function listData(){
        return file_get_contents('../app/file/data.txt');
    }

     /*** @return File      */
     public  static function getInstance(){
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

     private function closeLocalFile(){	fclose($this->localFile);	}

     function __construct(){  }
     protected function __clone(){}
}

